// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBEK4rH-oAF75Uc2czeKkUtZ4kQXRXHZzE",
    authDomain: "hex4-f7973.firebaseapp.com",
    databaseURL: "https://hex4-f7973.firebaseio.com",
    projectId: "hex4-f7973",
    storageBucket: "hex4-f7973.appspot.com",
    messagingSenderId: "452019693017"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
